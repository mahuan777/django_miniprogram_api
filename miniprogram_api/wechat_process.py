import base64
import json
from Crypto.Cipher import AES
from django.conf import settings
doc = "https://github.com/ti-technology/django_miniprogram_api"

'''
WeChat Crypt
'''
class WeChatCrypt:
    def __init__(self, appId, sessionKey):
        self.appId = appId
        self.sessionKey = sessionKey

    def decrypt(self, encryptedData, iv):
        # base64 decode
        sessionKey = base64.b64decode(self.sessionKey)
        encryptedData = base64.b64decode(encryptedData)
        iv = base64.b64decode(iv)
        cipher = AES.new(sessionKey, AES.MODE_CBC, iv)
        decrypted = json.loads(self._unpad(cipher.decrypt(encryptedData)))

        if decrypted['watermark']['appid'] != self.appId:
            raise Exception('Invalid Buffer')

        return decrypted

    def _unpad(self, s):
        return s[:-ord(s[len(s)-1:])]


'''
WeChat Pay Process
'''
import json
import random
import string
import hashlib

import xmltodict

import requests
from xml.etree.ElementTree import *


class WeChatPay:


    def __init__(self):
        if not settings.WECHAT_MINIPROGRAM_CONFIG.get("WECHAT_PAY", None):
            raise ValueError(f"Value WECHAT_PAY is required for this mini program, please check the doc {doc}")

        if not settings.WECHAT_MINIPROGRAM_CONFIG.get("WECHAT_PAY").get("MCH_ID",
                                                                        None) or settings.WECHAT_MINIPROGRAM_CONFIG.get(
                "WECHAT_PAY").get("MCH_ID", None) == "":
            raise ValueError(f"Value MCH_ID is required for WECHAT_PAY, please check the doc {doc}")

        if not settings.WECHAT_MINIPROGRAM_CONFIG.get("WECHAT_PAY").get("KEY",
                                                                        None) or settings.WECHAT_MINIPROGRAM_CONFIG.get(
                "WECHAT_PAY").get("KEY", None) == "":
            raise ValueError(f"Value KEY is required for WECHAT_PAY, please check the doc {doc}")

        if not settings.WECHAT_MINIPROGRAM_CONFIG.get("WECHAT_PAY").get("NOTIFICATION_URL",
                                                                        None) or settings.WECHAT_MINIPROGRAM_CONFIG.get(
                "WECHAT_PAY").get("NOTIFICATION_URL", None) == "":
            raise ValueError(f"Value NOTIFICATION_URL is required for WECHAT_PAY, please check the doc {doc}")
        self.appId = settings.WECHAT_MINIPROGRAM_CONFIG['APPID']
        self.secret = settings.WECHAT_MINIPROGRAM_CONFIG['SECRET']
        self.mch_id = settings.WECHAT_MINIPROGRAM_CONFIG["WECHAT_PAY"]['MCH_ID']
        self.notify_url = settings.WECHAT_MINIPROGRAM_CONFIG["WECHAT_PAY"]['NOTIFICATION_URL']

    def ranstr(self, num):
        salt = ''.join(random.sample(string.ascii_letters + string.digits, num))

        return salt

    def unified_order(self, open_id, body, order_id, total_fee, spbill_create_ip):
        nonce_str = self.ranstr(16)
        url = 'https://api.mch.weixin.qq.com/pay/unifiedorder'
        payload = {
            'appid': self.appId,
            'body': body,
            'mch_id': self.mch_id,
            'nonce_str': nonce_str,
            'notify_url': self.notify_url,
            'openid': open_id,
            'out_trade_no': str(order_id),
            'spbill_create_ip': spbill_create_ip,
            'trade_type': 'JSAPI',
            'total_fee': int(total_fee),
        }
        sign = WeChatSignHelper(payload, settings.WECHAT_MINIPROGRAM_CONFIG["WECHAT_PAY"]['KEY']).getSign()
        payload['sign'] = sign
        payload = str(self.dic_to_xml(payload))
        response = requests.post(url, data=payload.encode("utf-8"))
        data = self.xml_to_dict(response.content.decode())
        return data

    def order_query(self, transaction_id=None, out_trade_no=None):
        url = "https://api.mch.weixin.qq.com/pay/orderquery"
        nonce_str = self.ranstr(8)
        if transaction_id:
            string_for_sign = "appid=" +self.appId + "&mch_id=" + self.mch_id + "&nonce_str=" + nonce_str + "&transaction_id=" + transaction_id
        elif out_trade_no:
            string_for_sign = "appid=" +self.appId + "&mch_id=" + self.mch_id + "&nonce_str=" + nonce_str + "&out_trade_no=" + out_trade_no
        sign = string_for_sign + settings.WECHAT_MINIPROGRAM_CONFIG["WECHAT_PAY"]['KEY']
        sign = str(hashlib.md5(sign.encode())).upper()
        if transaction_id:
            payload = {
                'appid': self.appId,
                'mch_id': self.mch_id,
                'nonce_str': nonce_str,
                'sign': sign,
                'transaction_id': transaction_id
            }
        elif out_trade_no:
            payload = {
                'appid': self.appId,
                'mch_id': self.mch_id,
                'nonce_str': nonce_str,
                'out_trade_no': out_trade_no,
                'sign': sign,
            }

        response = requests.post(url, data=payload)
        return response.json()

    def close_order(self, out_trade_no):
        url = "https://api.mch.weixin.qq.com/pay/closeorder"
        nonce_str = self.ranstr(8)
        string_for_sign = "appid=" +self.appId + "&mch_id=" + self.mch_id + "&nonce_str=" + nonce_str + "&out_trade_no=" + out_trade_no
        sign = string_for_sign + settings.WECHAT_MINIPROGRAM_CONFIG["WECHAT_PAY"]['KEY']
        sign = str(hashlib.md5(sign.encode())).upper()
        payload = {
            'appid': self.appId,
            'mch_id': self.mch_id,
            'nonce_str': nonce_str,
            'out_trade_no': out_trade_no,            
            'sign': sign
        }
        
        #20200506 关闭订单post的是xml格式数据
        xmlload = str(self.dic_to_xml(payload))
        response = requests.post(url, data=xmlload.encode("utf-8"))
        data = self.xml_to_dict(response.content.decode())
        return data

    #申请退款
    def pay_refund(self,out_refund_no, out_trade_no,refund_fee,total_fee,transaction_id):
        url = "https://api.mch.weixin.qq.com/secapi/pay/refund"
        nonce_str = self.ranstr(8)
        string_for_sign = "appid=" +self.appId + "&mch_id=" + self.mch_id + "&nonce_str=" + nonce_str +"&out_refund_no" + out_refund_no
        string_for_sign = string_for_sign + "&out_trade_no" + out_trade_no + "&refund_fee" + refund_fee + "&total_fee" + total_fee + "&transaction_id" + transaction_id
        sign = string_for_sign + settings.WECHAT_MINIPROGRAM_CONFIG["WECHAT_PAY"]['KEY']
        sign = str(hashlib.md5(sign.encode())).upper()
        payload = {
            'appid': self.appId,
            'mch_id': self.mch_id,
            'nonce_str': nonce_str,
            'out_refund_no': out_refund_no,
            'out_trade_no': out_trade_no,
            'refund_fee': refund_fee,
            'total_fee': total_fee,
            'transaction_id': transaction_id,
            'sign': sign
        }

        xmlload = str(self.dic_to_xml(payload))
        response = requests.post(url, data=xmlload.encode("utf-8"))
        data = self.xml_to_dict(response.content.decode())
        return data

    #申请退款查询
    def pay_refund_query(self,out_refund_no, out_trade_no,transaction_id):
        url = "https://api.mch.weixin.qq.com/pay/refundquery"
        nonce_str = self.ranstr(8)
        string_for_sign = "appid=" +self.appId + "&mch_id=" + self.mch_id + "&nonce_str=" + nonce_str +"&out_refund_no=" + out_refund_no
        string_for_sign = string_for_sign + "&out_trade_no=" + out_trade_no + "&refund_id=" + "" + "&transaction_id=" + transaction_id
        sign = string_for_sign + settings.WECHAT_MINIPROGRAM_CONFIG["WECHAT_PAY"]['KEY']
        sign = str(hashlib.md5(sign.encode())).upper()
        payload = {
            'appid': self.appId,
            'mch_id': self.mch_id,
            'nonce_str': nonce_str,
            'out_refund_no': out_refund_no,
            'out_trade_no': out_trade_no,
            'refund_id': "",
            'total_fee': total_fee,
            'transaction_id': transaction_id,
            'sign': sign
        }

        xmlload = str(self.dic_to_xml(payload))
        response = requests.post(url, data=xmlload.encode("utf-8"))
        data = self.xml_to_dict(response.content.decode())
        return data

    #对账单下载
    def pay_downloadbill(self,bill_date):
    '''
    微信的说明：
    商户可以通过该接口下载历史交易清单。比如掉单、系统错误等导致商户侧和微信侧数据不一致，通过对账单核对后可校正支付状态。
    注意：
    1、微信侧未成功下单的交易不会出现在对账单中。支付成功后撤销的交易会出现在对账单中，跟原支付单订单号一致；
    2、微信在次日9点启动生成前一天的对账单，建议商户10点后再获取；
    3、对账单中涉及金额的字段单位为“元”。
    4、对账单接口只能下载三个月以内的账单。
    5、对账单是以商户号纬度来生成的，如一个商户号与多个appid有绑定关系，则使用其中任何一个appid都可以请求下载对账单。对账单中的appid取自交易时候提交的appid，与请求下载对账单时使用的appid无关。
    '''
        url = "https://api.mch.weixin.qq.com/pay/downloadbill"
        nonce_str = self.ranstr(8)
        string_for_sign = "appid=" +self.appId + "&bill_date=" + bill_date + "&mch_id="+ self.mch_id + "&bill_type=ALL"  + "&nonce_str=" + nonce_str
        sign = string_for_sign + settings.WECHAT_MINIPROGRAM_CONFIG["WECHAT_PAY"]['KEY']
        sign = str(hashlib.md5(sign.encode())).upper()
        payload = {
            'appid': self.appId,
            'bill_date':bill_date,
            'bill_type':"ALL",
            'mch_id': self.mch_id,
            'nonce_str': nonce_str,
            'sign': sign
        }

        xmlload = str(self.dic_to_xml(payload))
        response = requests.post(url, data=xmlload.encode("utf-8"))
        data = self.xml_to_dict(response.content.decode())
        return data

    #下载资金账单
    def pay_downloadfundflow(self,bill_date):
    '''
    商户可以通过该接口下载自2017年6月1日起 的历史资金流水账单。
    说明：
    1、资金账单中的数据反映的是商户微信账户资金变动情况；
    2、当日账单在次日上午9点开始生成，建议商户在上午10点以后获取；
    3、资金账单中涉及金额的字段单位为“元”。
    '''
        url = "https://api.mch.weixin.qq.com/pay/downloadfundflow"
        nonce_str = self.ranstr(8)
        string_for_sign = "appid=" +self.appId + "&bill_date=" + bill_date + "&account_type=Basic" + "&mch_id="+ self.mch_id + "&nonce_str=" + nonce_str
        sign = string_for_sign + settings.WECHAT_MINIPROGRAM_CONFIG["WECHAT_PAY"]['KEY']
        sign = str(hashlib.md5(sign.encode())).upper()
        payload = {
            'appid': self.appId,
            'bill_date':bill_date,
            'account_type':"Basic",
            'mch_id': self.mch_id,
            'nonce_str': nonce_str,
            'sign': sign
        }

        xmlload = str(self.dic_to_xml(payload))
        response = requests.post(url, data=xmlload.encode("utf-8"))
        data = self.xml_to_dict(response.content.decode())
        return data


    def dic_to_xml(self,d):
        ele = '<xml>'
        for key, val in d.items():
            ele += '<' + str(key) + '>' + str(val) + '</' + str(key) + '>'

        return ele + '</xml>'

    def xml_to_dict(self,xml):
        return xmltodict.parse(str(xml))['xml']


import collections
import hashlib
import copy

class WeChatSignHelper:
    def __init__(self,dataDict,apiKey):
        self.data = copy.deepcopy(dataDict) #type: dict
        self.apiKey = apiKey
        self.keyValueString = ""
        self.clean_up()
        self.dataToKeyValueString()


    def clean_up(self):
        if "sign" in self.data:
            del self.data["sign"]

    def dataToKeyValueString(self):
        od = collections.OrderedDict(sorted(self.data.items()))
        for key,value in od.items():
            self.keyValueString+=f"{key}={value}&"
        self.keyValueString += f"key={self.apiKey}"

    def getSign(self):
        md5Obj = hashlib.md5()
        md5Obj.update(self.keyValueString.encode("utf-8"))
        return md5Obj.hexdigest().upper()